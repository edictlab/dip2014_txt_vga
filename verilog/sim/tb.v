module tb;
  reg clk;
  reg rstbutton;
  reg [31:0] counter;
  initial begin
    clk = 0;
    rstbutton = 0;
    counter = 0;
  end
  always begin
    #1 clk = ~clk;
    counter = counter + 1;
    
  end

  top  top_inst(clk, rstbutton, 
//  btnupA, btndownA, btnupB, btndownB,
  Hsync, Vsync, vgaRed, vgaGreen, vgaBlue);

  initial begin

    $dumpfile("dump-buffer-test.vcd");
    $dumpvars(0,tb);

    $display ("time\t test");
    $monitor("%g \t  %d", $time, Hsync, Vsync);

    #6000 $finish;
  end
endmodule
