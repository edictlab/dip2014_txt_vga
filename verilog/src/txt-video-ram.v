// Dual port video text buffer
// Port A: from CPU       > write 8/16/24/32 bit with [2:0] wea
// Port B: from video if  > read 32 bit
module txt_video_ram(
  input  clka,
  input  reset,
  input       rea,
  input       [NB_COL-1:0]            wea,
  input       [ADDR_WIDTH-1:0]        addra,
  input       [NB_COL*COL_WIDTH-1:0]  dina,
  output [NB_COL*COL_WIDTH-1:0]  douta,
  output reg  dreadya = 0,
  input       clkb,
  input       reb,
  input       [ADDR_WIDTH-1:0]        addrb,
  output [NB_COL*COL_WIDTH-1:0]  doutb
  // output reg  dreadyb
  );

  reg [ADDR_WIDTH-1:0]        reg_addra;
  reg [ADDR_WIDTH-1:0]        reg_addrb;
  parameter BUFFER_WIDTH  = 80;
  parameter BUFFER_HEIGHT = 40;

  parameter ADDR_WIDTH = 18;
  parameter COL_WIDTH = 8;
  parameter NB_COL = 4;

  reg [NB_COL*COL_WIDTH - 1 : 0] 
      RAM [ BUFFER_WIDTH/NB_COL * BUFFER_HEIGHT - 1 : 0];

  // Initial: load initial buffer from file
  initial begin
    $readmemh("txt-32.data", RAM);
  end

  //--- Read RAM from port B
  always @(posedge clkb) begin
    if (reb) begin
      reg_addrb <= addrb;
    end
  end

  //--- Read/Write RAM from port A
  always@(posedge clka) begin
    if (reset)
      dreadya <= 0;
    else
      dreadya <= |wea | rea;
    reg_addra <= addra;
  end


  assign douta = RAM[reg_addra];
  assign doutb = RAM[reg_addrb];

  generate
  genvar i;
  for (i = 0; i < NB_COL; i = i+1)
  begin : PortA
    always @(posedge clka)
    begin
      if (wea[i])
        RAM    [addra] [(i+1)*COL_WIDTH-1:i*COL_WIDTH]
             <= dina  [(i+1)*COL_WIDTH-1:i*COL_WIDTH];
    end
  end
  endgenerate

endmodule
