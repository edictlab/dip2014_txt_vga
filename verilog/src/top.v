
module top(
  clk, rstbutton, 
  Hsync, Vsync, 
  vgaRed, vgaGreen, vgaBlue
);
  
    input clk, rstbutton;
    output Hsync, Vsync;
    output reg [2:0] vgaRed;
    output reg [2:0] vgaGreen;
    output reg [1:0] vgaBlue;

    // Clock signals
    wire clock, clock2x, vga_clk;
    wire PLL_Locked;

    reg reset_n;
    always @(posedge clock) begin
        reset_n <= rstbutton | ~PLL_Locked;
    end

    // Clock Generation
    clk_gen Clock_Generator (
        .CLK_IN1  (clk),
        .RESET       (1'b0),
        .CLK_OUT1  (clock), 
        .CLK_OUT2  (clock2x),
        .CLK_OUT3  (vga_clk),
        .LOCKED   (PLL_Locked)
    );

    //----------------------------------------------------
    // Video buffer je 80 znakova * 40 redova = 3200 bajta
    // 80 znakova u redu = 20 4-bajtnih rijeci
    // U narednim linijama je primjer jedno-bajtnog upisa (znakova) u bafer.
    // Na gornjoj polovici ekrana ispisuju se znakovi ABCD...

    wire [31:0] podatak = 32'h41424344;         // slova ABCD
    wire [17:0] adresa;
    reg [3:0] wenable = 0;
    wire  re = 0;           // ne citamo iz buffera, nego upisujemo
    
    reg [17:0] counter = 18'h000000; 
    assign adresa = counter[17:2];

    always @(*) begin
      if(reset_n)
        wenable = 0;
      else
        if(counter < 1600)
          case( counter[1:0])
            2'b00: wenable = 4'b1000;
            2'b01: wenable = 4'b0100;
            2'b10: wenable = 4'b0010;
            default: wenable = 4'b0001;
          endcase
          else
            wenable = 4'b0000;
    end

    always @(posedge clock2x)
      if(reset_n)
        counter <= 0;
      else
        if(counter < 3200)
          counter <= counter + 1;
        else
          counter <= 0;
    //----------------------------------------------------
    // Inicijalni sadrzaj txt video bafera je dat u fajlu txt-32.data
    // Zamijenite sadrzaj tog fajla sa sadrzajem fajla txt-32-fe.data (ili
    // izvrsite rename fajla). Dobicete drugaciji pocetni sadrzaj bafera.
    //----------------------------------------------------

    wire inDisplayArea;
    wire [11:0] x;           // vga x coordinate
    // wire [11:0] y;        // vga y coordinate
    supply1 font_read_en;    // always read font data 

    wire [7:0] font_data; 
    wire [31:0] ram_data;
    wire [17:0] font_addr, ram_addr;

    wire [31:0] dout;

  txt_video_ram screen_buffer(
    .clka   (clock2x),     // input clka
    .reset   (reset_n),    // input rsta
    .rea    (re),          // input  rea
    .wea    (wenable),     // input [3 : 0] wea
    .addra  (adresa),      // input [ADDR_WIDTH-1 : 0] addra
    .dina   (podatak),     // input [31 : 0] dina
    .douta   (dout),       // output [31 : 0] douta
    .dreadya (dready),
    .clkb   (vga_clk),         // input clkb 
    .reb    (font_read_en),    // input reb
    .addrb  (ram_addr),        // input [ADDR_WIDTH-1 : 0] addrb
    .doutb  (ram_data)         // output [31 : 0] doutb
    );

    txt_vga_logic text_vga_logic_inst(
      .vgaclk     (vga_clk),
      .vsync      (Vsync),
      .x          (x),
      .on_screen  (inDisplayArea),
      .ram_addr   (ram_addr), // goes to bufer
      .ram_data   (ram_data), // comes from buffer
      .rom_addr   (font_addr), // goes to rom
      .rom_data   (font_data), // comes from rom
      .pixel_out  (pixel_out)  // defines pixel ON or OFF 
    );

    font_rom fonts(
      .clk    (vga_clk),
      .re     (font_read_en), 
      .addr   (font_addr), // comes from logic
      .dout   (font_data)  // goes to logic
    );

    vga640x480 vga_inst(
      .vgaclk     (vga_clk),
      .reset      (reset_n),
      .hsync      (Hsync),
      .vsync      (Vsync),
      .x          (x),
      // .y          (y),
      .inrange    (inDisplayArea)
    );
    
    always @(posedge vga_clk)
    begin
      if(inDisplayArea)
        if( pixel_out)
        begin
            // Yellow
            vgaRed    <= 3'b111;
            vgaGreen  <= 3'b111;
            vgaBlue   <= 2'b00;
        end
        else
        begin
            //Blue
            vgaRed    <= 3'b00;
            vgaGreen  <= 3'b000;
            vgaBlue   <= 2'b11;
        end
      else
      begin
        // Black (turn electron beam off)
        vgaRed    <= 3'b000;
        vgaGreen  <= 3'b000;
        vgaBlue   <= 2'b00;
      end
    end

endmodule
