# Textualni video interfejs

U repozitoriju je Verilog projekat u kome je realizovan ispis teksta na ekran. 
Radi jednostavnosti primjera upotrebe modula realizovanih unutar ovog Verilog projekta, na gornju polovicu ekrana ispisuju se karakteri 'A', 'B', 'C', 'D' jedan po jedan (ASCII kodovi 0x41, 0x42, 0x43, 0x44).

Projekat se sastoji iz sljedećih modula:

 * `vga640x480` - realizovan u fajlu `vga.v`. Ovaj modul služi za generisanje sinhronizacionih signala za VGA i korišten je u zadaći 2.

 * `txt_video_ram` - realizovan u fajlu `txt-video-ram.v`. U ovom modulu je realizovana dual-portna memorija za čuvanje teksta koji treba da se ispisuje na ekranu. Tekst na ekranu je organizovan u 40 redova sa po 80 znakova u redu, što čini ukupno 3200 znakova, tj. 3200 bajta memorije. U svaki bajt se upisuje ASCII kod znaka koji želimo da se prikaže na odgovarajućem mjestu na ekranu. Sa stanovišta upisivanja teksta na ekran, ova memorija je organizovana kao 32-bitna memorija, što znači da ima 800 32-bitnih lokacija. Upisivanje u memoriju (port A) se vrši tako što se pošalje adresa 32 bitne lokacije (`addra`), 32 bitni podatak (`dina`) i 4-bitni write-enable signal (`wea`) koji govori koji bajt (bajti) iz 32-bitnog podatka će se upisati u memoriju. Sa strane porta A moguće je i čitati ovu memoriju zadavanjem adrese i read-enable signala (`rea`). Sadržaj 32-bitne memorijske lokacije koja se čita će biti dat preko signala douta. Uočite da se adresira 32-bitni podatak, tako da je adresa prva četiri znaka gore lijevo 0, zadnja četiri znaka gore desno je 19, adresa prva četiri znaka u drugom redu je 20 itd. Adresa zadnja četiri znaka u donjem desnom uglu je 799. Drugi port (B) ove memorije služi za čitanje memorije po određenom pravilu sa ciljem generisanja signala za VGA. Inicijalni sadržaj ove memorije se učitava iz fajla txt-32.data.

 * `font_rom` - realizovan u fajlu `font-rom.v`. Ovaj modul predstavlja 8-bitnu read-only memoriju za čuvanje izgleda karaktera za prikaz na ekranu. Karakteri su organizovani kao matrice bita 8x12. Za svaki znak se koristi 12 bajta koji predstavljaju 12 redova sa po 8 bita (tačkica). Izgled za svih 256 znakova ASCII tabele je definisan u fajlu `font-def.data`.

 * `txt_vga_logic` - realizovan u fajlu `txt-vga-logic.v`. Sva logika u vezi sa dohvatanjem odgovarajućih podataka iz RAM i ROM memorije u kojima se nalaze definicije znaka koji se ispisuje i sinhronizacija sa VGA signalima se nalazi u ovom modulu.


Sinteza ovog projekta se vrši komandom `make` iz root foldera projekta, a učitavanje generisanog bit fajla na FPGA čip komandom `make download`.
